
CREATE CONSTRAINT ON (a:Asset) assert a.id is unique;


:auto USING PERIODIC COMMIT
LOAD CSV WITH HEADERS FROM "file:///asset_header.csv" AS small
WITH small WHERE small.gics_sector IS NOT NULL
and small.risk_country IS NOT NULL
MERGE (a:Asset {id: small.isin,
    sector: (small.gics_sector),
risk: (small.risk_country)});


MATCH ()-[r:CORR_1M]->()
DELETE r

:auto USING PERIODIC COMMIT
LOAD CSV WITH HEADERS FROM "file:///correlations_1m.csv" AS corrs
with corrs, corrs.isin1 as col1, corrs.isin2 as col2, corrs.correlations as c_vals
MATCH (n:Asset {id: col1})
MATCH (n2:Asset {id: col2})
MERGE (n)-[:CORR_1M {onemcorr: toFloat(c_vals)}]-(n2)


MATCH (n:Asset {id: 'RU0007775219'})
CALL gds.alpha.spanningTree.kmin.write({
  nodeProjection: 'Asset',
  relationshipProjection: {
    LINK: {
      type: 'CORR_1M',
      properties: 'onemcorr'},
    k:20,
    startNodeId: id(n),
    relationshipWeightProperty: 'onemcorr',
    writeProperty: 'kminst'
  }
})
YIELD createMillis, computeMillis, writeMillis, effectiveNodeCount
RETURN createMillis,computeMillis,writeMillis, effectiveNodeCount;



MATCH (n:Place{id: 'D'})
CALL gds.alpha.spanningTree.kmin.write({
  nodeProjection: 'Place',
  relationshipProjection: {
    LINK: {
      type: 'LINK',
      properties: 'cost'
    }
  },
  k: 3,
  startNodeId: id(n),
  relationshipWeightProperty: 'cost',
  writeProperty:'kminst'
})
YIELD createMillis, computeMillis, writeMillis, effectiveNodeCount
RETURN createMillis,computeMillis,writeMillis, effectiveNodeCount
